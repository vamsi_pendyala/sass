# MySassApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2. The style sheets are of Sass/Scss format instead of Css.
Sass facilitates you to write clean, easy and less CSS in a programming construct.
It contains fewer codes so you can write CSS quicker.
It is more stable, powerful, and elegant because it is an extension of CSS. So, it is easy for designers and developers to work more efficiently and quickly.
It is compatible with all versions of CSS. So, you can use any available CSS libraries.
It provides nesting so you can use nested syntax and useful functions like color manipulation, math functions and other value
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
